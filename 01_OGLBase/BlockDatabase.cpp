//main includes
#include "BlockDatabase.h"

BlockDatabase::BlockDatabase() : textureAtlas("atlas")
{
    m_blocks[(int)BlockID::Air] = std::make_unique<DefaultBlock>("Air");
    m_blocks[(int)BlockID::Grass] = std::make_unique<DefaultBlock>("Grass");
    m_blocks[(int)BlockID::Dirt] = std::make_unique<DefaultBlock>("Dirt");
    m_blocks[(int)BlockID::Stone] = std::make_unique<DefaultBlock>("Stone");
    m_blocks[(int)BlockID::Bedrock] = std::make_unique<DefaultBlock>("Bedrock");
}

BlockDatabase& BlockDatabase::get()
{
    static BlockDatabase d;
    return d;
}

const BlockType& BlockDatabase::getBlock(BlockID id) const
{
    return *m_blocks[(int)id];
}

const BlockData& BlockDatabase::getData(BlockID id) const
{
    return m_blocks[(int)id]->getData();
}
