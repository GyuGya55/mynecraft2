#pragma once

//main includes
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <vector>
#include <stdexcept>

//additional includes
#include "BasicTexture.h"


class TextureAtlas : public BasicTexture 
{
    public:
        TextureAtlas(const std::string& textureFileName);

        std::vector<GLfloat> getTexture(const glm::vec2& coords);

    protected:
        int m_imageSize;
        int m_individualTextureSize;
};