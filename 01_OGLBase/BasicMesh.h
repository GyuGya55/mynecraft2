#pragma once

//main includes
#include <GL/glew.h>
#include <vector>

//additional includes


struct BasicMesh
{
	std::vector<GLfloat> vertexPositions;
	std::vector<GLfloat> textureCoords;
	std::vector<GLuint> indices;
};
