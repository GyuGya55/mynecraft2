#pragma once

//main includes
#include <GL/glew.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <vector>
#include <iostream>

//additional includes
#include "ChunkSection.h"
#include "ChunkMesh.h"
#include "ChunkMeshBuilder.h"

#include "BlockDatabase.h"
#include "BlockData.h"


class ChunkSection;
class ChunkMesh;
class BlockData;
class BlockDataHolder;

class ChunkMeshBuilder
{
    public:
        ChunkMeshBuilder(ChunkSection& chunk);

        void buildMesh(ChunkMesh& Mesh);

    private:
        void tryAddFaceToMesh
        (
            const std::vector<GLfloat>& blockFace,
            const glm::vec2& textureCoords,
            const glm::vec3& blockPosition,
            const glm::vec3& blockFacing
        );

        bool shouldMakeFace
        (
            const glm::vec3& blockPosition,
            const BlockDataHolder& blockData
        );

        int faces = 0;

        ChunkSection* m_pChunk;
        ChunkMesh* m_pMesh;

        const BlockDataHolder* m_pBlockData;
};

namespace
{
    const std::vector<GLfloat> frontFace
    {
        0, 0, 1,
        1, 0, 1,
        1, 1, 1,
        0, 1, 1,
    };

    const std::vector<GLfloat> backFace
    {
        1, 0, 0,
        0, 0, 0,
        0, 1, 0,
        1, 1, 0,
    };

    const std::vector<GLfloat> leftFace
    {
        0, 0, 0,
        0, 0, 1,
        0, 1, 1,
        0, 1, 0,
    };

    const std::vector<GLfloat> rightFace
    {
        1, 0, 1,
        1, 0, 0,
        1, 1, 0,
        1, 1, 1,
    };

    const std::vector<GLfloat> topFace
    {
        0, 1, 1,
        1, 1, 1,
        1, 1, 0,
        0, 1, 0,
    };

    const std::vector<GLfloat> bottomFace
    {
        0, 0, 0,
        1, 0, 0,
        1, 0, 1,
        0, 0, 1
    };

}