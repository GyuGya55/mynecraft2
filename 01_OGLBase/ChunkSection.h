#pragma once

//main include
#include <array>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

//additional include
#include "ChunkDetails.h"
#include "ChunkMesh.h"
#include "ChunkBlock.h"
#include "BlockID.h"


class ChunkSection
{
public:
    ChunkSection();

    void setBlock(int x, int y, int z, ChunkBlock block);
    ChunkBlock getBlock(int x, int y, int z) const;

    const glm::vec3 getLocation() const;

    ChunkMesh mesh;

private:
    static bool outOfBoundsV(int value);
    static bool outOfBoundsH(int value);
    static int  getIndex(int x, int y, int z);

    std::array<ChunkBlock, CHUNK_VOLUME> m_blocks;

    glm::vec3 m_location;
};