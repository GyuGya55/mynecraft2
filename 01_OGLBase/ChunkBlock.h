#pragma once

//main includes

//additional includes
#include "BlockID.h"
#include "BlockDatabase.h"

class BlockData;
class BlockType;

struct ChunkBlock
{
    ChunkBlock() = default;

    ChunkBlock(Block_t id);
    ChunkBlock(BlockID id);

    const BlockData& getData() const;
    const BlockType& getType() const;

    bool operator == (ChunkBlock other) { return id == other.id; }

    Block_t id = 1;
};