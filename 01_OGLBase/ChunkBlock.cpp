#include "ChunkBlock.h"


ChunkBlock::ChunkBlock(Block_t id) : id(id) { }

ChunkBlock::ChunkBlock(BlockID id) : id(static_cast<Block_t>(id)) { }

const BlockData& ChunkBlock::getData() const { return BlockDatabase::get().getData((BlockID)id); }

const BlockType& ChunkBlock::getType() const { return BlockDatabase::get().getBlock((BlockID)id); }