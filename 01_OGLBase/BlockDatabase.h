#pragma once

//main include
#include <vector>
#include <array>

//additional include
#include "BlockType.h"
#include "BlockID.h"
#include "TextureAtlas.h"

class BlockDatabase
{
public:
    static BlockDatabase& get();

    const BlockType& getBlock(BlockID id) const;
    const BlockData& getData(BlockID id) const;

    TextureAtlas textureAtlas;

private:
    BlockDatabase();

    std::array<std::unique_ptr<BlockType>, (unsigned)BlockID::NUM_TYPES> m_blocks;
};