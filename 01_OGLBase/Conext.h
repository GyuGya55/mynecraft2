#pragma once

//main includes
#include <SDL.h>

//additional includes


struct Context
{
    Context();

    SDL_Window* window;
    SDL_Renderer* renderer;
};