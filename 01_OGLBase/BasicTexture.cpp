//main includes
#include "BasicTexture.h"


BasicTexture::BasicTexture(const std::string& file) { loadFromFile(file); }

BasicTexture::~BasicTexture() { glDeleteTextures(1, &m_id); }

void BasicTexture::loadFromImage(const Texture2D& image)
{
    glGenTextures(1, &m_id);
    glBindTexture(GL_TEXTURE_2D, m_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);


    /*int imageWidth, imageHeight;
    int miplevel = 0;
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &imageWidth);
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &imageHeight);

    std::cout << imageWidth << " " << imageHeight << std::endl;

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, GLsizei(image), GLsizei(image), 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 1);*/
}

void BasicTexture::loadFromFile(const std::string& file)
{
    Texture2D image;
    image.FromFile(file + ".png");

    loadFromImage(image);
}

void BasicTexture::bindTexture() const { glBindTexture(GL_TEXTURE_2D, m_id); }