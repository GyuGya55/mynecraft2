#pragma once

#include <GL/glew.h>
#include <vector>
#include "includes/TextureObject.h"
#include <glm/glm.hpp>

class BlockRender
{
public:
	BlockRender();
	~BlockRender();

	void Init();
	void Draw();

	struct Vertex {
		glm::vec3 p;
		glm::vec2 t;
	};

	/*void AddVertex(const Vertex& vert) {
		vertices.push_back(vert);
	}

	void AddIndex(unsigned int ind) {
		indices.push_back(vertices.size() - 4 + ind);
	}*/

protected:
	std::vector<Vertex> vertices;
	std::vector<int> indices;
	Texture2D grass_texture;

	GLuint vao;
	GLuint vbo;
	GLuint ib;
};


