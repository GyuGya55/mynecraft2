#pragma once

//main includes
#include <glm/glm.hpp>
#include <vector>

//additional includes
#include "TextureAtlas.h"
#include "BasicTexture.h"
#include "BasicShader.h"
#include "Model.h"
#include "Matrix.h"
#include "Camera.h"


class Camera;

class CubeRender
{
public:
    CubeRender();

    void add(const glm::vec3& position);

    void render(glm::mat4 viewProj);

private:
    std::vector<glm::vec3> m_quads;

    Model m_cubeModel;
    BasicShader m_shader;
    BasicTexture m_basicTexture;

    TextureAtlas m_atlasTest;
};