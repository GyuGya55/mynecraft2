//main includes
#include "QuadRender.h"


QuadRender::QuadRender()
{
    m_basicTexture.loadFromFile("assets/rock");

    m_quadModel.addData
    (
        {
    
            {
                -0.5,  0.5,  0,
                 0.5,  0.5,  0,
                 0.5, -0.5,  0,
                -0.5, -0.5,  0,
            },
            {
                0, 1,
                1, 1,
                1, 0,
                0, 0,
            },
            {
                0, 1, 2,
                2, 3, 0
            }
        }
    );
}

void QuadRender::add(const glm::vec3& position)
{
    m_quads.push_back(position);
}

void QuadRender::renderQuads(glm::mat4 viewProj)//const Camera& camera)
{
    m_shader.useProgram();
    m_quadModel.bindVAO();
    m_basicTexture.bindTexture();

    //m_shader.loadProjectionViewMatrix(camera.getProjectionViewMatrix());
    m_shader.loadProjectionViewMatrix(viewProj);

    for (auto& quad : m_quads)
    {
        m_shader.loadModelMatrix(makeModelMatrix({ quad, {0, 0, 0} }));

        glDrawElements(GL_TRIANGLES, m_quadModel.getIndicesCount(), GL_UNSIGNED_INT, nullptr);
    }

    m_quads.clear();
}