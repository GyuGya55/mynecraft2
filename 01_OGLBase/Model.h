#pragma once

//main includes
#include <GL/glew.h>
#include <vector>

//additional includes
#include "BasicMesh.h"


class Model
{
	public:
		Model() = default;
		Model(const BasicMesh& mesh);

		~Model();

		void addData(const BasicMesh& mesh);
		void deleteData();

		void addVBO(int dimensions, const std::vector<GLfloat>& data);
		void bindVAO() const;

		int getIndicesCount() const;

	private:
		void addEBO(const std::vector<GLuint>& indices);

		GLuint m_vao = 0;
		int m_vboCount = 0;
		int m_indicesCount = 0;
		std::vector<GLuint> m_buffers;
};