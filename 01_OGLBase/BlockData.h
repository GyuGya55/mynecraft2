#pragma once

//main includes
#include <string>
#include <fstream>

//additional includes
#include <glm/vec2.hpp>

struct BlockDataHolder
{
    glm::vec2 texTopCoord;
    glm::vec2 texSideCoord;
    glm::vec2 texBottomCoord;
};

class BlockData
{
public:
    BlockData(const std::string& fileName);

    const BlockDataHolder& getBlockData() const;

private:
    BlockDataHolder m_data;

};