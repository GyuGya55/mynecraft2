#pragma once

//main includes
#include <glm/glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

//additional includes
#include "Entity.h"
#include "Camera.h"


class Camera;
class Entity;

glm::mat4 makeModelMatrix(const Entity& entity);
glm::mat4 makeViewMatrix(const Camera& camera);
glm::mat4 makeProjectionMatrix(float fov);