#pragma once

//main includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>
#include <iostream>

#include "Application.h"

//additional includes
#include "BasicShader.h"
#include "BasicTexture.h"
#include "Model.h"
#include "Camera.h"
#include "Entity.h"
#include "Matrix.h"


class Camera;

class QuadRender
{
    public:
        QuadRender();

        void add(const glm::vec3& position);

        void renderQuads(glm::mat4 viewProj);//const Camera& camera);

    private:
        std::vector<glm::vec3> m_quads;

        Model m_quadModel;
        BasicShader m_shader;
        BasicTexture m_basicTexture;
};