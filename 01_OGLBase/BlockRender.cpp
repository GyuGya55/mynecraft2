#include <GL\glew.h>
#include <SDL_opengl.h>
#include "BlockRender.h"
#include <iostream>


BlockRender::BlockRender() {}


BlockRender::~BlockRender()
{
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
    glDeleteBuffers(1, &ib);
}


void BlockRender::Init()
{
    std::vector<Vertex>vertices;
	
	//front					
    vertices.push_back({ glm::vec3(0, 0, 1), glm::vec2(0, 0) });
    vertices.push_back({ glm::vec3(1, 0, 1), glm::vec2(1, 0) });
    vertices.push_back({ glm::vec3(1, 1, 1), glm::vec2(0, 1) });
    vertices.push_back({ glm::vec3(0, 1, 1), glm::vec2(1, 1) });
	//back
    vertices.push_back({ glm::vec3(1, 0, 0), glm::vec2(0, 0) });
    vertices.push_back({ glm::vec3(0, 0, 0), glm::vec2(1, 0) });
    vertices.push_back({ glm::vec3(0, 1, 0), glm::vec2(0, 1) });
    vertices.push_back({ glm::vec3(1, 1, 0), glm::vec2(1, 1) });
	//right									 
	vertices.push_back({ glm::vec3(1, 0, 1), glm::vec2(0, 0) });
	vertices.push_back({ glm::vec3(1, 0, 0), glm::vec2(1, 0) });
	vertices.push_back({ glm::vec3(1, 1, 0), glm::vec2(0, 1) });
	vertices.push_back({ glm::vec3(1, 1, 1), glm::vec2(1, 1) });
	//left									 
	vertices.push_back({ glm::vec3(0, 0, 0), glm::vec2(0, 0) });
	vertices.push_back({ glm::vec3(0, 0, 1), glm::vec2(1, 0) });
	vertices.push_back({ glm::vec3(0, 1, 1), glm::vec2(0, 1) });
	vertices.push_back({ glm::vec3(0, 1, 0), glm::vec2(1, 1) });
	//top									 
	vertices.push_back({ glm::vec3(0, 1, 1), glm::vec2(0, 0) });
	vertices.push_back({ glm::vec3(1, 1, 1), glm::vec2(1, 0) });
	vertices.push_back({ glm::vec3(1, 1, 0), glm::vec2(0, 1) });
	vertices.push_back({ glm::vec3(0, 1, 0), glm::vec2(1, 1) });
	//bottom								 
	vertices.push_back({ glm::vec3(0, 0, 0), glm::vec2(0, 0) });
	vertices.push_back({ glm::vec3(1, 0, 0), glm::vec2(1, 0) });
	vertices.push_back({ glm::vec3(1, 0, 1), glm::vec2(0, 1) });
	vertices.push_back({ glm::vec3(0, 0, 1), glm::vec2(1, 1) });



    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, grass_texture);

    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ib);

    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertices.size(), (void*)&vertices[0], GL_STREAM_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(glm::vec3)));

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(), (void*)&indices[0], GL_STREAM_DRAW);

    glBindVertexArray(0);
}


void BlockRender::Draw()
{
	GLuint indices[36];
	int index = 0;
	//4 cs�cspontonk�nt 6 index elt�rol�sa
	for (int i = 0; i < 6 * 4; i += 4)
	{
		indices[index + 0] = i + 0;
		indices[index + 1] = i + 1;
		indices[index + 2] = i + 2;
		indices[index + 3] = i + 1;
		indices[index + 4] = i + 3;
		indices[index + 5] = i + 2;
		index += 6;
	}

    glBindVertexArray(vao);

    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);
}