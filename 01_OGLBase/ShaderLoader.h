#pragma once

//main includes
#include <GL/glew.h>
#include <string>
#include <stdexcept>
#include <fstream>
#include <sstream>
#include <iostream>

//additional includes


GLuint loadShaders(const std::string& vertexShader, const std::string& fragmentShader);

std::string getFileContents(const std::string& filePath);

namespace
{
    GLuint compileShader(const GLchar* source, GLenum shaderType)
    {
        auto shaderID = glCreateShader(shaderType);

        glShaderSource(shaderID, 1, &source, nullptr);
        glCompileShader(shaderID);

        GLint isSuccess = 0;
        GLchar infoLog[512];

        glGetShaderiv(shaderID, GL_COMPILE_STATUS, &isSuccess);
        if (!isSuccess)
        {
            glGetShaderInfoLog(shaderID, 512, nullptr, infoLog);
            throw std::runtime_error("Unable to load a shader: " + std::string(infoLog));
        }

        return shaderID;
    }

    GLuint linkProgram(GLuint vertexShaderID, GLuint fragmentShaderID)
    {
        auto id = glCreateProgram();

        glAttachShader(id, vertexShaderID);
        glAttachShader(id, fragmentShaderID);

        std::cout << vertexShaderID << " " << fragmentShaderID << std::endl;

        glBindAttribLocation(id, 0, "inVertexPosition");
        glBindAttribLocation(id, 1, "inTextureCoord");

        glLinkProgram(id);

        return id;
    }
}