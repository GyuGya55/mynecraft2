#pragma once

//main includes
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <vector>
#include <iostream>

//additional includes
#include "BasicMesh.h"
#include "Model.h"
#include "ChunkDetails.h"


class ChunkMesh
{
public:
	ChunkMesh();
	
	void addFace
	(
		const std::vector<GLfloat>& blockFace,
		const std::vector<GLfloat>& textureCoords,
		const glm::vec3& chunkPosition,
		const glm::vec3& blockPosition
	);

	void BufferMesh();

	const Model& getModel() const;

private:
	BasicMesh m_mesh;
	Model m_model;
	GLuint m_indexIndex = 0;
};