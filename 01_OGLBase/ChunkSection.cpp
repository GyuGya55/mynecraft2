//main includes
#include "ChunkSection.h"


ChunkSection::ChunkSection() { }

void ChunkSection::setBlock(int x, int y, int z, ChunkBlock block)
{
    if (outOfBoundsH(x)) return;
    if (outOfBoundsV(y)) return;
    if (outOfBoundsH(z)) return;

    m_blocks[getIndex(x, y, z)] = block;
}

ChunkBlock ChunkSection::getBlock(int x, int y, int z) const
{
    if (outOfBoundsH(x)) return BlockID::Air;
    if (outOfBoundsV(y)) return BlockID::Air;;
    if (outOfBoundsH(z)) return BlockID::Air;;

    return m_blocks[getIndex(x, y, z)];
}

const glm::vec3 ChunkSection::getLocation() const
{
    return m_location;
}

bool ChunkSection::outOfBoundsH(int value)
{
    return  value >= CHUNK_SIZE_HORIZONTAL || value < 0;
}

bool ChunkSection::outOfBoundsV(int value)
{
    return  value >= CHUNK_SIZE_VERTICAL || value < 0;
}

int ChunkSection::getIndex(int x, int y, int z)
{
    return  y * CHUNK_AREA + z * CHUNK_SIZE_VERTICAL + x;
}
