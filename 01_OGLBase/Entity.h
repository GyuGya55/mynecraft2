#pragma once

//main includes
#include "Matrix.h"

//additional includes

struct Entity
{
    glm::vec3 position;
    glm::vec3 rotation;
};
