#pragma once

//main includes

//additional includes
#include "Shader.h"


class BasicShader : public Shader
{
    public:
        BasicShader(const std::string& vertexFile = "BasicVertex", const std::string& fragmentFile = "BasicFragment");

        void loadProjectionViewMatrix(const glm::mat4& pvMatrix);
        void loadModelMatrix(const glm::mat4& matrix);

    protected:
        virtual void getUniforms() override;

    private:
        GLuint m_locationProjectionViewMatrix;
        GLuint m_locationModelMatrix;
};
