#pragma once

//main includes
#include <GL/glew.h>
#include "includes/TextureObject.h"
#include <iostream>
#include <SDL.h>

//additional includes


class BasicTexture
{
    public:
        BasicTexture() = default;
        BasicTexture(const std::string& file);

        ~BasicTexture();

        void loadFromImage(const Texture2D& image);
        void loadFromFile(const std::string& file);


        void bindTexture() const;

    private:
        GLuint m_id;
};