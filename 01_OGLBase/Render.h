#pragma once

//main includes
#include "Includes/TextureObject.h"
#include <SDL.h>

//additional includes
#include "QuadRender.h"
#include "CubeRender.h"
#include "ChunkRender.h"



class Camera;
class ChunkMesh;

class Render
{
    public:
        //void drawQuad(const glm::vec3& pos);
        //void drawCube(const glm::vec3& pos);
        //void drawChunk(const ChunkMesh& mesh);

        void finishRender(SDL_Window& window, const Camera& camera);
        void finishRender();

    private:
        //QuadRender m_quadRenderer;
        //CubeRender m_cubeRenderer;
        //ChunkRender m_chunkRenderer;
};