#include "Application.h"

#include <math.h>
#include <vector>

#include <array>
#include <list>
#include <tuple>
#include <imgui/imgui.h>
#include "includes/GLUtils.hpp"
#include "BasicTexture.h"

CApplication::CApplication(void)
{
	m_camera.SetView(glm::vec3(5, 5, 5), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
}

CApplication::~CApplication(void)
{
	std::cout << "dtor!\n";
}

void CApplication::InitSkyBox()
{
	m_SkyboxPos.BufferData(
		std::vector<glm::vec3>{
		// h�ts� lap
		glm::vec3(-1, -1, -1),
		glm::vec3(1, -1, -1),
		glm::vec3(1, 1, -1),
		glm::vec3(-1, 1, -1),
		// el�ls� lap
		glm::vec3(-1, -1, 1),
		glm::vec3(1, -1, 1),
		glm::vec3(1, 1, 1),
		glm::vec3(-1, 1, 1),
	}
	);

	// �s a primit�veket alkot� cs�cspontok indexei (az el�z� t�mb�kb�l) - triangle list-el val� kirajzol�sra felk�sz�lve
	m_SkyboxIndices.BufferData(
		std::vector<int>{
			// h�ts� lap
			0, 1, 2,
			2, 3, 0,
			// el�ls� lap
			4, 6, 5,
			6, 4, 7,
			// bal
			0, 3, 4,
			4, 3, 7,
			// jobb
			1, 5, 2,
			5, 6, 2,
			// als�
			1, 0, 4,
			1, 4, 5,
			// fels�
			3, 2, 6,
			3, 6, 7,
	}
	);

	// geometria VAO-ban val� regisztr�l�sa
	m_SkyboxVao.Init(
		{
			{ CreateAttribute<0, glm::vec3, 0, sizeof(glm::vec3)>, m_SkyboxPos },
		}, m_SkyboxIndices
	);

	// skybox texture
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

	glGenTextures(1, &m_skyboxTexture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_skyboxTexture);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	TextureFromFileAttach("assets/xpos.png", GL_TEXTURE_CUBE_MAP_POSITIVE_X);
	TextureFromFileAttach("assets/xneg.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_X);
	TextureFromFileAttach("assets/ypos.png", GL_TEXTURE_CUBE_MAP_POSITIVE_Y);
	TextureFromFileAttach("assets/yneg.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_Y);
	TextureFromFileAttach("assets/zpos.png", GL_TEXTURE_CUBE_MAP_POSITIVE_Z);
	TextureFromFileAttach("assets/zneg.png", GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

}

void CApplication::CoutTest()
{
}


glm::vec3 GetGroundPos(float u, float v)
{
	return glm::vec3(-u * 40 + 20, (0.5 * (sinf(u*30)+cosf(v*30))) + (0.1 * ((u * 40 - 20) + (v * 40 - 20))), v * 40 - 20);
}

glm::vec3 GetGroundNorm(float u, float v)
{
	glm::vec3 du = GetGroundPos(u + 0.01, v) - GetGroundPos(u - 0.01, v);
	glm::vec3 dv = GetGroundPos(u, v + 0.01) - GetGroundPos(u, v - 0.01);

	return glm::normalize(glm::cross(du, dv));
}

glm::vec2 GetGroundTex(float u, float v)
{
	return glm::vec2(1 - u, 1 - v);
}

void CApplication::InitGround()
{
	int N = 39, M = 39;
	std::vector<Vertex> vert((N + 1) * (M + 1));
	for (int i = 0; i <= N; ++i)
	{
		for (int j = 0; j <= M; ++j)
		{
			float u = i / (float)N;
			float v = j / (float)M;

			vert[i + j * (N + 1)].p = GetGroundPos(u, v);
			vert[i + j * (N + 1)].n = GetGroundNorm(u, v);
			vert[i + j * (N + 1)].t = GetGroundTex(u, v);
		}
	}

	std::vector<int> indices(3 * 2 * (N) * (M));
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < M; ++j)
		{
			indices[6 * i + j * 3 * 2 * (N)+0] = (i)+(j) * (N + 1);
			indices[6 * i + j * 3 * 2 * (N)+1] = (i + 1) + (j) * (N + 1);
			indices[6 * i + j * 3 * 2 * (N)+2] = (i)+(j + 1) * (N + 1);
			indices[6 * i + j * 3 * 2 * (N)+3] = (i + 1)+(j) * (N + 1);
			indices[6 * i + j * 3 * 2 * (N)+4] = (i + 1)+(j + 1) * (N + 1);
			indices[6 * i + j * 3 * 2 * (N)+5] = (i)+(j + 1) * (N + 1);
		}
	}

	m_GroundPos.BufferData(vert);

	m_GroundIndices.BufferData(indices);

	m_GroundVao.Init(
		{
			{ CreateAttribute <	0, glm::vec3, 0, sizeof(Vertex) >, m_GroundPos},
			{ CreateAttribute < 1, glm::vec3, (sizeof(glm::vec3)), sizeof(Vertex)>, m_GroundPos},
			{ CreateAttribute < 2, glm::vec2, (2 * sizeof(glm::vec3)), sizeof(Vertex)>, m_GroundPos},
		},
		m_GroundIndices
	);
}

void CApplication::InitShaders()
{
	/*// a shadereket t�rol� program l�trehoz�sa az OpenGL-hez hasonl� m�don:
	m_program.AttachShaders({
		{ GL_VERTEX_SHADER, "BasicVertex.vert"},
		{ GL_FRAGMENT_SHADER, "BasicFragment.frag"}
	});

	// attributomok osszerendelese a VAO es shader kozt
	m_program.BindAttribLocations({
		{ 0, "vs_in_pos" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
		{ 1, "vs_in_norm" },			// VAO 1-es csatorna menjen a vs_in_norm-ba
		{ 2, "vs_in_tex" },				// VAO 2-es csatorna menjen a vs_in_tex-be
	});

	m_program.LinkProgram();

	// shader program r�vid l�trehoz�sa, egyetlen f�ggv�nyh�v�ssal a fenti h�rom:
	m_programSkybox.Init(
		{
			{ GL_VERTEX_SHADER, "skybox.vert" },
			{ GL_FRAGMENT_SHADER, "skybox.frag" }
		},
		{
			{ 0, "vs_in_pos" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
		}
	);*/
}

bool CApplication::Init()
{
	// t�rl�si sz�n legyen k�kes
	glClearColor(0.125f, 0.25f, 0.5f, 1.0f);

	//glEnable(GL_CULL_FACE); // kapcsoljuk be a hatrafele nezo lapok eldobasat
	glEnable(GL_DEPTH_TEST); // m�lys�gi teszt bekapcsol�sa (takar�s)

	InitShaders();
	InitSkyBox();

	InitGround();

	/*// egy�b text�r�k bet�lt�se
	m_grassTexture.FromFile("atlas.png");
	m_rockTexture.FromFile("assets/rock.jpg");
	*/
	// kamera
	m_camera.SetProj(45.0f, 640.0f / 480.0f, 0.01f, 1000.0f);

	return true;
}

void CApplication::Clean()
{
	glDeleteTextures(1, &m_skyboxTexture);
}

void CApplication::Update()
{
	static Uint32 last_time = SDL_GetTicks();
	float delta_time = (SDL_GetTicks() - last_time) / 1000.0f;

	m_camera.Update(delta_time);

	last_time = SDL_GetTicks();
}

void CApplication::Render()
{
	//SDL_ShowCursor(0);
	// t�r�lj�k a frampuffert (GL_COLOR_BUFFER_BIT) �s a m�lys�gi Z puffert (GL_DEPTH_BUFFER_BIT)
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	m_program.Use();
	glm::mat4 viewProj = m_camera.GetViewProj();
	glm::mat4 world = glm::mat4(1.0f);
	
	QuadRender quad;
	quad.add({ 5, 5, 5 });
	//quad.add({ 2, 2, 2 });
	//quad.add({ 10, 10, 10 });
	quad.renderQuads(viewProj);

	/*CubeRender cube;
	cube.add({ 10, 5, 5 });
	cube.render(viewProj);*/

	/*glEnable(GL_TEXTURE_2D);s

	//ground
	m_GroundVao.Bind();
	glUniform1i(glGetUniformLocation(m_program, "texImage1"), 0);
	glUniform1i(glGetUniformLocation(m_program, "texImage2"), 1);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_grassTexture);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_rockTexture);

	m_program.SetUniform("MVP", viewProj);
	m_program.SetUniform("world", glm::mat4(1));
	m_program.SetUniform("worldIT", glm::mat4(1));
	//glDrawElements(GL_TRIANGLES, 39 * 39 * 3 * 2, GL_UNSIGNED_INT, nullptr);
	GL::drawElements(39 * 39 * 3 * 2);
	m_program.Unuse();

	// skybox
	eltol�s 
	// most kisebb-egyenl�t haszn�ljunk, mert mindent kitolunk a t�voli v�g�s�kokra
	glDepthFunc(GL_LEQUAL);

	GL::bindVao(m_SkyboxVao);
	m_programSkybox.Use();
	m_programSkybox.SetUniform("MVP", viewProj * glm::translate( m_camera.GetEye()) );
	
	// cube map text�ra be�ll�t�sa 0-�s mintav�telez�re �s annak a shaderre be�ll�t�sa
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_skyboxTexture);
	glUniform1i(m_programSkybox.GetLocation("skyboxTexture"), 0);
	// az el�z� h�rom sor <=> m_programSkybox.SetCubeTexture("skyboxTexture", 0, m_skyboxTexture);

	//glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
	GL::drawElements(36);
	m_programSkybox.Unuse();*/
}

void CApplication::KeyboardDown(SDL_KeyboardEvent& key)
{
	m_camera.KeyboardDown(key);
}

void CApplication::KeyboardUp(SDL_KeyboardEvent& key)
{
	m_camera.KeyboardUp(key);
}

void CApplication::MouseMove(SDL_MouseMotionEvent& mouse)
{
	m_camera.MouseMove(mouse);
}

void CApplication::MouseDown(SDL_MouseButtonEvent& mouse)
{
}

void CApplication::MouseUp(SDL_MouseButtonEvent& mouse)
{
}

void CApplication::MouseWheel(SDL_MouseWheelEvent& wheel)
{
}

// a k�t param�terbe az �j ablakm�ret sz�less�ge (_w) �s magass�ga (_h) tal�lhat�
void CApplication::Resize(int _w, int _h)
{
	glViewport(0, 0, _w, _h );

	m_camera.Resize(_w, _h);
}
