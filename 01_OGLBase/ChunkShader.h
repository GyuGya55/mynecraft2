#pragma once

//main includes

//additional includes
#include "BasicShader.h"


class ChunkShader : public BasicShader
{
    public:
        ChunkShader();

    private:
        void getUniforms() override;
};