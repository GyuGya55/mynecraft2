#pragma once

#include <memory>
#include <GL/glew.h>
#include <SDL.h>
#include <SDL_opengl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>
#include "includes/gCamera.h"
#include "includes/ProgramObject.h"
#include "includes/BufferObject.h"
#include "includes/VertexArrayObject.h"
#include "includes/TextureObject.h"
#include "includes/ObjParser_OGL3.h"
#include "GLFunctions.h"

#include "BlockRender.h"
#include "Render.h"
#include "CubeRender.h"
#include "ChunkRender.h"
#include "ChunkMesh.h"

class CApplication
{
public:
	CApplication(void);
	~CApplication(void);

	bool Init();
	void Clean();

	void Update();
	void Render();
	void CoutTest();

	void KeyboardDown(SDL_KeyboardEvent&);
	void KeyboardUp(SDL_KeyboardEvent&);
	void MouseMove(SDL_MouseMotionEvent&);
	void MouseDown(SDL_MouseButtonEvent&);
	void MouseUp(SDL_MouseButtonEvent&);
	void MouseWheel(SDL_MouseWheelEvent&);
	void Resize(int, int);

protected:
	// shaderekhez sz�ks�ges v�ltoz�k
	ProgramObject		m_program;
	ProgramObject		m_programSkybox;
	VertexArrayObject	m_SkyboxVao;
	IndexBuffer			m_SkyboxIndices;	
	ArrayBuffer			m_SkyboxPos;

	VertexArrayObject	m_GroundVao;
	IndexBuffer			m_GroundIndices;
	ArrayBuffer			m_GroundPos;

	gCamera				m_camera;

	Texture2D			m_grassTexture;
	Texture2D			m_rockTexture;

	// nyers OGL azonos�t�k
	GLuint				m_skyboxTexture;




	struct Vertex
	{
		glm::vec3 p;
		glm::vec3 n;
		glm::vec2 t;
	};

	// a jobb olvashat�s�g kedv��rt
	void InitShaders();
	void InitSkyBox();

	void InitGround();
};

