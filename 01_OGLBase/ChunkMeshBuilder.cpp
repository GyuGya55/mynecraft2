//main includes
#include "ChunkMeshBuilder.h"


ChunkMeshBuilder::ChunkMeshBuilder(ChunkSection& chunk) : m_pChunk(&chunk) { }

struct Directions
{
    void update(int x, int y, int z)
    {
        up = { x,     y + 1,  z };
        down = { x,     y - 1,  z };
        left = { x - 1, y,      z };
        right = { x + 1, y,      z };
        front = { x,     y,      z + 1 };
        back = { x,     y,      z - 1 };
    }

    glm::vec3 up;
    glm::vec3 down;
    glm::vec3 left;
    glm::vec3 right;
    glm::vec3 front;
    glm::vec3 back;
};

void ChunkMeshBuilder::buildMesh(ChunkMesh& mesh)
{
    m_pMesh = &mesh;

    Directions directions;

    for (int8_t y = 0; y < CHUNK_SIZE_VERTICAL; ++y)
        for (int8_t x = 0; x < CHUNK_SIZE_HORIZONTAL; ++x)
            for (int8_t z = 0; z < CHUNK_SIZE_HORIZONTAL; ++z)
            {
                glm::vec3 position(x, y, z);
                auto  block = m_pChunk->getBlock(x, y, z);
                if (block == 0)
                {
                    continue;
                }

                m_pBlockData = &block.getData().getBlockData();
                auto& data = *m_pBlockData;
                directions.update(x, y, z);

                tryAddFaceToMesh(topFace, data.texTopCoord, position, directions.up);
                tryAddFaceToMesh(bottomFace, data.texBottomCoord, position, directions.down);
                tryAddFaceToMesh(leftFace, data.texSideCoord, position, directions.left);
                tryAddFaceToMesh(rightFace, data.texSideCoord, position, directions.right);
                tryAddFaceToMesh(frontFace, data.texSideCoord, position, directions.front);
                tryAddFaceToMesh(backFace, data.texSideCoord, position, directions.back);
            }
}

void ChunkMeshBuilder::tryAddFaceToMesh
(
    const std::vector<GLfloat>& blockFace,
    const glm::vec2& textureCoords,
    const glm::vec3& blockPosition,
    const glm::vec3& blockFacing
)
    {
        if (shouldMakeFace(blockFacing, *m_pBlockData))
        {
            faces++;
            auto texCoords = BlockDatabase::get().textureAtlas.getTexture(textureCoords);

            m_pMesh->addFace(blockFace, texCoords, m_pChunk->getLocation(), blockPosition);
        }
    }

bool ChunkMeshBuilder::shouldMakeFace
(
    const glm::vec3& adjBlock,
    const BlockDataHolder& blockData
)
    {
        auto block = m_pChunk->getBlock(adjBlock.x, adjBlock.y, adjBlock.z);
        //auto& data  = block.getData();

        if (block == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }