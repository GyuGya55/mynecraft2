#pragma once

//main includes
#include <vector>
#include <iostream>

//additional includes
#include "ChunkShader.h"
#include "ChunkMesh.h"
#include "BlockDatabase.h"
#include "Camera.h"

class ChunkMesh;
class Camera;

class ChunkRender
{
public:
    void add(const ChunkMesh& mesh);
    void render(glm::mat4 viewProj);

private:
    std::vector<const ChunkMesh*> m_chunks;

    ChunkShader m_shader;

};

