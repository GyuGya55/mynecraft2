//main includes
#include "ChunkRender.h"


void ChunkRender::add(const ChunkMesh& mesh) { m_chunks.push_back(&mesh); }

void ChunkRender::render(glm::mat4 viewProj)
{
    glEnable(GL_CULL_FACE);
    m_shader.useProgram();
    BlockDatabase::get().textureAtlas.bindTexture();

    //m_shader.loadProjectionViewMatrix(camera.getProjectionViewMatrix());
    m_shader.loadProjectionViewMatrix(viewProj);

    for (const ChunkMesh* mesh : m_chunks)
    {
        const ChunkMesh& m = *mesh;

        m.getModel().bindVAO();
        glDrawElements(GL_TRIANGLES, m.getModel().getIndicesCount(), GL_UNSIGNED_INT, nullptr);
    }

    m_chunks.clear();
}